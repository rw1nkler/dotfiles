VIM_DIR=`pwd`/vim
EMACS_DIR=`pwd`/emacs
TMUX_DIR=`pwd`/tmux
ZSH_DIR=`pwd`/zsh

help:
	echo "This will be help!"

### TMUX ###

tmux-install:
	cd ${TMUX_DIR}; bash install.sh

tmux-uninstall:
	cd ${TMUX_DIR}; bash uninstall.sh

### VIM ###

vim-install:
	cd ${VIM_DIR}; bash install.sh

vim-uninstall:
	cd ${VIM_DIR}; bash uninstall.sh

### ZSH ###

zsh:
	cd $ZSH_DIR; bash install.sh

everything:
	make emacs
	make tmux
	make vim
	make zsh

