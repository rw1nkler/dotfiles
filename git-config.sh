#!/bin/bash

git config --global user.name "Robert Winkler"
git config --global user.email rwinkler@antmicro.com

git config --global core.editor vi
git config --global commit.template ~/.gitcommit.txt

git config --global diff.tool vimdiff
git config --global difftool.prompt false
